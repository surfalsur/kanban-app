import React from 'react';
import classnames from 'classnames';

const Editable = ({editing, value, onEdit,className}) => {
  if(editing) {
    return <Editable.Edit value={value} onEdit={onEdit} className={className} />;
  }

  return <Editable.Value value={value} className={className}/>;
};


Editable.Value = ({value,className, ...props}) => <span className={classnames('value',className)} {...props}>{value}</span>


class Edit extends React.Component {

  render() {
    const {className,value,onEdit,...props} = this.props;

    return <input type="text"
      className={classnames('edit',className)}
      autoFocus={true}
    defaultValue={value}
    onBlur={this.finishEdit}
    onKeyPress={this.checkEnter}
    {...props}/>;

  }

  checkEnter = (e) => {
    if(e.key === 'Enter') {
      this.finishEdit(e);
    }


  }

  finishEdit = (e) => {
    const value = e.target.value;
    if(this.props.onEdit) {
      this.props.onEdit(value);
    }

  }

}
Editable.Edit = Edit;

export default Editable;
