import React from 'react'
import {DragSource,DropTarget} from 'react-dnd';
import ItemTypes from '../constants/itemTypes';
import {compose} from 'redux';


const Note = ({
  connectDragSource,connectDropTarget,children,...props
}) => {
  return compose(connectDragSource,connectDropTarget)(
    <div {...props}>
      {children}
    </div>
  );
};

const noteSource = {
  beginDrag(props) {
      console.log('begin drag and drop',props);
      return {};
  }
};

const noteTarget = {
  hover(targetProps,monitor) {
    const sourceProps = monitor.getItem();

    console.log('draging note',sourceProps,targetProps);
  }
};

export default compose(
  DragSource(ItemTypes.NOTE,noteSource,connect => ({
      connectDragSource: connect.dragSource()
  })),
  DropTarget(ItemTypes.NOTE,noteTarget,connect => ({
      connectDropTarget: connect.dropTarget()
  }))
)(Note)
